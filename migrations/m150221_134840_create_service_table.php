<?php

use yii\db\Schema;
use yii\db\Migration;

class m150221_134840_create_service_table extends Migration
{
    public function up()
    {
	    $this->createTable('service', [
		    'id' => Schema::TYPE_PK,
		    'name' => Schema::TYPE_STRING . '(50) NOT NULL',
		    'nick' => Schema::TYPE_STRING . '(50) NOT NULL',
		    'many_sites' => Schema::TYPE_BOOLEAN . ' NOT NULL DEFAULT 0 COMMENT "Многосайтовость"',
		    'creator_id' => Schema::TYPE_INTEGER,
		    'created_at' => Schema::TYPE_INTEGER . ' NOT NULL',
		    'updated_at' => Schema::TYPE_INTEGER . ' NOT NULL',
	    ]);
	    $this->addForeignKey('user_id_FK_service', 'service', 'creator_id', 'user', 'id', 'SET NULL', 'CASCADE');
    }

    public function down()
    {
        echo "m150221_134840_create_service_table cannot be reverted.\n";

        return false;
    }
}
