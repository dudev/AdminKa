<?php

namespace app\controllers\diet;

use app\extensions\Controller;

class SiteController extends Controller {
	public $service = 'diet';
    public function actionIndex()
    {
        return $this->render('index');
    }

}
