<?php
/**
 * Project: Admin - Seven lights
 * User: Evgeny Rusanov
 * E-mail: admin@dudev.ru
 * Site: dudev.ru
 */

namespace app\controllers\diet;


use app\extensions\Controller;
use app\models\forms\diet\CategoryOfProductForm;
use app\models\search\CategoryOfProductSearch;
use general\controllers\api\Controller as ApiController;
use general\ext\api\diet\DietApi;
use yii\web\NotFoundHttpException;
use yii\web\Response;

class CategoryOfProductController extends Controller {
	public $service = 'diet';
	public function actionIndex() {
		$searchModel = new CategoryOfProductSearch();
		$dataProvider = $searchModel->search([
			'CategoryOfArticleSearch' => \Yii::$app->request->get('CategoryOfArticleSearch', []),
			'sort' => \Yii::$app->request->get('sort', ''),
		]);

		return $this->render('index', [
			'dataProvider' => $dataProvider,
			'searchModel' => $searchModel,
		]);
	}
	public function actionUpdate($id) {
		$model = new CategoryOfProductForm();
		$model->isNewRecord = false;

		$res = DietApi::categoryOfProductUpdate($id);
		if($res['result'] == 'error') {
			throw new NotFoundHttpException();
		}
		$model->setAttributes($res['category']);

		if(\Yii::$app->request->isPost) {
			$model->load(\Yii::$app->request->post());
			if($model->validate()) {
				$res = DietApi::categoryOfProductUpdate(
					$id,
					$model->getAttributes([
						'name',
						'number',
						'parent_id',
					])
				);
				if($res['result'] == 'success') {
					$this->redirect(['update', 'id' => $id]);
				} else {
					$errors = [
						ApiController::ERROR_UNKNOWN => 'info',
						ApiController::ERROR_DB => 'info',
						ApiController::ERROR_ILLEGAL_REQUEST_METHOD => 'info',
						ApiController::ERROR_NO_DATA => 'info',

						ApiController::ERROR_ILLEGAL_CATEGORY_OF_ARTICLE_NAME => 'name',
						ApiController::ERROR_ILLEGAL_CATEGORY_OF_ARTICLE_NUMBER => 'number',
						ApiController::ERROR_ILLEGAL_CATEGORY_OF_ARTICLE_PARENT_ID => 'parent_id',
					];
					foreach ($res['errors'] as $error) {
						if($error['code'] == ApiController::ERROR_NO_CATEGORY_OF_ARTICLE) {
							throw new NotFoundHttpException();
						}
						if(isset($errors[ $error['code'] ])) {
							$model->addError($errors[ $error['code'] ], $error['title']);
						} else {
							$model->addError('unknown', ApiController::$_errors[ ApiController::ERROR_UNKNOWN ]);
						}
					}
				}
			}
		}

		return $this->render('update', [
			'model' => $model,
		]);
	}
	public function actionCreate() {
		$model = new CategoryOfProductForm();

		if(\Yii::$app->request->isPost) {
			$model->load(\Yii::$app->request->post());
			if($model->validate()) {
				$res = DietApi::categoryOfProductCreate(
					$model->getAttributes([
						'name',
						'number',
						'parent_id',
					])
				);
				if($res['result'] == 'success') {
					$this->redirect(['index']);
				} else {
					$errors = [
						ApiController::ERROR_UNKNOWN => 'info',
						ApiController::ERROR_DB => 'info',
						ApiController::ERROR_ILLEGAL_REQUEST_METHOD => 'info',
						ApiController::ERROR_NO_DATA => 'info',

						ApiController::ERROR_ILLEGAL_CATEGORY_OF_ARTICLE_NAME => 'name',
						ApiController::ERROR_ILLEGAL_CATEGORY_OF_ARTICLE_NUMBER => 'number',
						ApiController::ERROR_ILLEGAL_CATEGORY_OF_ARTICLE_PARENT_ID => 'parent_id',
					];
					foreach ($res['errors'] as $error) {
						if($error['code'] == ApiController::ERROR_NO_SITE) {
							throw new NotFoundHttpException();
						}
						if(isset($errors[ $error['code'] ])) {
							$model->addError($errors[ $error['code'] ], $error['title']);
						} else {
							$model->addError('unknown', ApiController::$_errors[ ApiController::ERROR_UNKNOWN ]);
						}
					}
				}
			}
		}

		return $this->render('create', [
			'model' => $model,
		]);
	}
	public function actionDelete($id) {
		DietApi::categoryOfProductDelete($id);
		$this->redirect(['index']);
	}
	public function actionMaxNumber($parent_id) {
		\Yii::$app->response->format = Response::FORMAT_JSON;
		$res = DietApi::categoryOfProductMaxNumber($parent_id);
		if($res['result'] == 'error') {
			return ['max' => 1];
		} else {
			return ['max' => $res['max_number'] ];
		}
	}
} 