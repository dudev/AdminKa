<?php

namespace app\controllers\system;

use app\extensions\Controller;
use app\models\Service;
use app\extensions\Permissions;
use Yii;
use app\models\Menu;
use app\models\search\MenuSearch;
use yii\base\Exception;
use yii\db\Expression;
use yii\db\Query;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;

/**
 * MenuController implements the CRUD actions for Menu model.
 */
class MenuController extends Controller {
	public $service = 'system';
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Menu models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new MenuSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Menu model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

	/**
	 * Creates a new Menu model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 * @throws Exception
	 * @throws \Exception
	 * @throws \yii\db\Exception
	 * @return mixed
	 */
    public function actionCreate() {
        $model = new Menu();

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
	        $this->prepareCrtUpd($model);
	        if(!$model->hasErrors()) {
		        $transaction = Yii::$app->db->beginTransaction();
		        try {
			        $this->moveDown($model);
			        $model->save(false);
			        $transaction->commit();
		        } catch (Exception $e) {
			        $transaction->rollBack();
			        throw $e;
		        }
		        return $this->redirect(['index']);
	        }
        }
	    return $this->render('create', [
		    'model' => $model,
	    ]);
    }

	/**
	 * Updates an existing Menu model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id
	 * @throws Exception
	 * @throws NotFoundHttpException
	 * @throws \Exception
	 * @throws \yii\db\Exception
	 * @return mixed
	 */
    public function actionUpdate($id) {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
	        $this->prepareCrtUpd($model);
	        if(!$model->hasErrors()) {
		        $transaction = Yii::$app->db->beginTransaction();
		        try {
			        if ($model->getOldAttribute('number') != $model->number) {
				        $this->moveUp($model);
				        $this->moveDown($model);
			        }
			        $model->save(false);
			        $transaction->commit();
		        } catch (Exception $e) {
			        $transaction->rollBack();
			        throw $e;
		        }
		        return $this->redirect(['index']);
	        }
        }
	    return $this->render('update', [
		    'model' => $model,
	    ]);
    }

	private function prepareCrtUpd(Menu $model) {
		$route = explode('?', $model->route);

		if($model->route != '#') {
			$url = explode('/', $route[0]);

			$service = Service::findOne($model->service_id);
			if (!in_array($url[1], explode(',', $service->controllers), true)) {
				$model->addError('route', 'Указанный контроллер не найден в выбранном сервисе');
			}

			if (!$model->permission) {
				if ($permission = Permissions::getPermissionFromAction($url[2])) {
					$model->permission = $permission;
				} else {
					$model->addError('permission', 'Не удалось определить автоматически');
				}
			}
		} else {
			if (!$model->permission) {
				$model->permission = Permissions::PERMISSION_READ;
			}
		}

		$params = [];
		if(isset($route[1])) {
			foreach (explode('&', $route[1]) as $val) {
				$param = explode('=', $val);
				$params[ $param[0] ] = isset($param[1]) ? $param[1] : '';
			}
		}
		$model->route = json_encode(
			array_merge(
				[$route[0]],
				$params
			)
		);
	}

	/**
	 * Поднимает пункты меню
	 * @param Menu $model
	 * @throws \yii\db\Exception
	 */
	private function moveUp(Menu $model) {
		(new Query())
			->createCommand()
			->update(
				Menu::tableName(),
				[
					'number' => new Expression('number - 1')
				],
				[
					'and',
					['>', 'number', $model->number],
					['service_id' => $model->service_id, 'parent_id' => $model->parent_id],
				]
			)
			->execute();
	}
	/**
	 * Упускает пункты меню
	 * @param Menu $model
	 * @throws \yii\db\Exception
	 */
	private function moveDown(Menu $model) {
		(new Query())
			->createCommand()
			->update(
				Menu::tableName(),
				[
					'number' => new Expression('number + 1')
				],
				[
					'and',
					['>=', 'number', $model->number],
					['service_id' => $model->service_id, 'parent_id' => $model->parent_id],
				]
			)
			->execute();
	}

	/**
	 * Deletes an existing Menu model.
	 * If deletion is successful, the browser will be redirected to the 'index' page.
	 * @param integer $id
	 * @throws Exception
	 * @throws NotFoundHttpException
	 * @throws \Exception
	 * @throws \yii\db\Exception
	 * @return mixed
	 */
    public function actionDelete($id)
    {
	    $model = $this->findModel($id);
	    $transaction = Yii::$app->db->beginTransaction();
	    try {
		    $this->moveUp($model);
		    $model->delete();
		    $transaction->commit();
	    } catch(Exception $e) {
		    $transaction->rollBack();
		    throw $e;
	    }

        return $this->redirect(['index']);
    }

	public function actionGetNumbers($service_id, $parent_id, $is_new_record) {
		Yii::$app->response->format = Response::FORMAT_JSON;
		return Menu::getNumbers($service_id, $parent_id, $is_new_record);
	}
	public function actionGetParents($service_id, $is_new_record) {
		Yii::$app->response->format = Response::FORMAT_JSON;
		return Menu::getParents($service_id, $is_new_record);
	}

    /**
     * Finds the Menu model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Menu the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Menu::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
