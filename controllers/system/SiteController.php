<?php

namespace app\controllers\system;

use app\extensions\Controller;

class SiteController extends Controller {
	public $service = 'system';
    public function actionIndex()
    {
        return $this->render('index');
    }
}
