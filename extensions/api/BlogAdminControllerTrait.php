<?php
/**
 * Project: Diet (My balanced diet) - Seven Lights
 * User: Evgeny Rusanov
 * E-mail: admin@dudev.ru
 * Site: dudev.ru
 */

namespace app\extensions\api;


use app\models\forms\blog\PostForm;
use app\models\search\PostSearch;
use general\controllers\api\Controller;
use general\ext\api\blog\BlogApi;
use yii\web\NotFoundHttpException;
use yii\web\UploadedFile;

trait BlogAdminControllerTrait {
	public function actionIndex($page = 0) {
		if(empty(static::$domain)) {
			throw new \InvalidArgumentException();
		}

		$searchModel = new PostSearch();
		$dataProvider = $searchModel->search([
			'domain' => static::$domain,
			'page' => $page,
			'PostSearch' => \Yii::$app->request->get('PostSearch', []),
			'sort' => \Yii::$app->request->get('sort', ''),
		]);

		return $this->render('index', [
			'dataProvider' => $dataProvider,
			'searchModel' => $searchModel,
		]);
	}
	public function actionUpdate($id) {
		if(empty(static::$domain)) {
			throw new \InvalidArgumentException();
		}

		$model = new PostForm();
		$model->isNewRecord = false;

		$res = BlogApi::postUpdate(static::$domain, $id);
		if($res['result'] == 'error') {
			throw new NotFoundHttpException();
		}
		$model->setAttributes($res['post']);
		$model->image_links = isset($res['post']['image_links']) ? $res['post']['image_links'] : null;
		$model->image_is_uploaded = $model->picture;

		if(\Yii::$app->request->isPost) {
			$model->load(\Yii::$app->request->post());
			$model->image = UploadedFile::getInstance($model, 'image');
			if($model->validate()) {
				$res = BlogApi::postUpdate(
					static::$domain,
					$id,
					array_merge(
						$model->getAttributes([
							'nick',
							'title',
							'keywords',
							'description',
							'text',
							'marks',
							'status',
							'publish_at',
							'picture',
							'pic_in_post'
						]),
						$model->image
							? ['image' => curl_file_create(
								$model->image->tempName,
								$model->image->type,
								$model->image->baseName . '.' . $model->image->extension)
							]
							: ['image' => '']
					)
				);
				if($res['result'] == 'success') {
					if($model->image && file_exists($model->image->tempName)) {
						unlink($model->image->tempName);
					}
					$this->redirect(['update', 'id' => $id]);
				} else {
					$errors = [
						Controller::ERROR_UNKNOWN => 'info',
						Controller::ERROR_DB => 'info',
						Controller::ERROR_ILLEGAL_REQUEST_METHOD => 'info',
						Controller::ERROR_NO_DATA => 'info',

						Controller::ERROR_ILLEGAL_NICK => 'nick',
						Controller::ERROR_ILLEGAL_TITLE => 'title',
						Controller::ERROR_ILLEGAL_KEYWORDS => 'keywords',
						Controller::ERROR_ILLEGAL_DESCRIPTION => 'description',
						Controller::ERROR_ILLEGAL_TEXT => 'text',
						Controller::ERROR_ILLEGAL_TAGS => 'marks',
						Controller::ERROR_ILLEGAL_POST_STATUS => 'status',
						Controller::ERROR_ILLEGAL_PUBLISH_DATE => 'publish_at',
						Controller::ERROR_ILLEGAL_FIELD_PICTURE => 'picture',
						Controller::ERROR_ILLEGAL_FIELD_PICTURE_IN_POST => 'pic_in_post',
						Controller::ERROR_ILLEGAL_PICTURE => 'image',
					];
					foreach ($res['errors'] as $error) {
						if($error['code'] == Controller::ERROR_NO_SITE
						|| $error['code'] == Controller::ERROR_NO_POST) {
							throw new NotFoundHttpException();
						}
						if(isset($errors[ $error['code'] ])) {
							$model->addError($errors[ $error['code'] ], $error['title']);
						} else {
							$model->addError('unknown', Controller::$_errors[ Controller::ERROR_UNKNOWN ]);
						}
					}
				}
			}
		}

		return $this->render('update', [
			'model' => $model,
		]);
	}
	public function actionCreate() {
		if(empty(static::$domain)) {
			throw new \InvalidArgumentException();
		}
		$model = new PostForm();

		if(\Yii::$app->request->isPost) {
			$model->load(\Yii::$app->request->post());
			$model->image = UploadedFile::getInstance($model, 'image');
			if($model->validate()) {
				$res = BlogApi::postCreate(
					static::$domain,
					array_merge(
						$model->getAttributes([
							'nick',
							'title',
							'keywords',
							'description',
							'text',
							'marks',
							'status',
							'publish_at',
							'picture',
							'pic_in_post'
						]),
						$model->image
							? ['image' => curl_file_create(
							$model->image->tempName,
							$model->image->type,
							$model->image->baseName . '.' . $model->image->extension)
						]
							: ['image' => '']
					)
				);
				if($res['result'] == 'success') {
					if($model->image && file_exists($model->image->tempName)) {
						unlink($model->image->tempName);
					}
					$this->redirect(['index']);
				} else {
					$errors = [
						Controller::ERROR_UNKNOWN => 'info',
						Controller::ERROR_DB => 'info',
						Controller::ERROR_ILLEGAL_REQUEST_METHOD => 'info',
						Controller::ERROR_NO_DATA => 'info',

						Controller::ERROR_ILLEGAL_NICK => 'nick',
						Controller::ERROR_ILLEGAL_TITLE => 'title',
						Controller::ERROR_ILLEGAL_KEYWORDS => 'keywords',
						Controller::ERROR_ILLEGAL_DESCRIPTION => 'description',
						Controller::ERROR_ILLEGAL_TEXT => 'text',
						Controller::ERROR_ILLEGAL_TAGS => 'marks',
						Controller::ERROR_ILLEGAL_POST_STATUS => 'status',
						Controller::ERROR_ILLEGAL_PUBLISH_DATE => 'publish_at',
						Controller::ERROR_ILLEGAL_FIELD_PICTURE => 'picture',
						Controller::ERROR_ILLEGAL_FIELD_PICTURE_IN_POST => 'pic_in_post',
						Controller::ERROR_ILLEGAL_PICTURE => 'image',
					];
					foreach ($res['errors'] as $error) {
						if($error['code'] == Controller::ERROR_NO_SITE) {
							throw new NotFoundHttpException();
						}
						if(isset($errors[ $error['code'] ])) {
							$model->addError($errors[ $error['code'] ], $error['title']);
						} else {
							$model->addError('unknown', Controller::$_errors[ Controller::ERROR_UNKNOWN ]);
						}
					}
				}
			}
		}

		return $this->render('create', [
			'model' => $model,
		]);
	}
	public function actionDelete($id) {
		if(empty(static::$domain)) {
			throw new \InvalidArgumentException();
		}
		BlogApi::postDelete(static::$domain, $id);
		$this->redirect(['index']);
	}
}