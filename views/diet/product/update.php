<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\forms\ProductForm */

$this->title = 'Изменить продукт';
$this->params['breadcrumbs'][] = ['label' => 'Моё сбалансированное питание', 'url' => ['diet/site/index']];
$this->params['breadcrumbs'][] = ['label' => 'Продукты', 'url' => ['diet/product/index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="product-update">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
