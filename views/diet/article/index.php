<?php
/**
 * Project: Blog Platform - Seven Lights
 * User: Evgeny Rusanov
 * E-mail: admin@dudev.ru
 * Site: dudev.ru
 */
use app\models\search\PostSearch;
use yii\grid\ActionColumn;
use yii\helpers\Html;

/* @var $this yii\web\View */
$this->title = 'Управление статьями';
$this->params['breadcrumbs'][] = 'Все статьи';

/* @var yii\data\ArrayDataProvider $dataProvider */
/* @var PostSearch $searchModel */
?>

<div class="menu-index">
	<p>
		<?= Html::a('Добавить статьи', ['create'], ['class' => 'btn btn-success']) ?>
	</p>

	<?= \yii\grid\GridView::widget([
		'dataProvider' => $dataProvider,
		'filterModel' => $searchModel,
		'layout' => '{items}{pager}',
		'columns' => [
			[
				'label' => $searchModel->getAttributeLabel('id'),
				'attribute' => 'id',
				'filterInputOptions' => ['style' => 'width: 50px;'],
			],
			[
				'label' => $searchModel->getAttributeLabel('title'),
				'attribute' => 'title',
			],
			[
				'label' => $searchModel->getAttributeLabel('status'),
				'attribute' => 'status',
				'value' => function($data) {
					/* @var $data array */
					if($data['status'] == PostSearch::STATUS_PUBLISHED
					&& $data['publish_at'] > time()) {
						return PostSearch::$statuses[ PostSearch::STATUS_WAITING ];
					}
					return PostSearch::$statuses[ $data['status'] ];
				},
				'filter' => PostSearch::$statuses,
			],
			[
				'label' => $searchModel->getAttributeLabel('publish_at'),
				'attribute' => 'publish_at',
				'format' => ['date', 'php:' . Yii::$app->params['dateFormat'] ],
			],
			[
				'class' => ActionColumn::className(),
				'template' => '{update} {delete}'
			],
		],
	]) ?>
</div>