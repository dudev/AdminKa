<?php
/* @var $this yii\web\View */
/* @var $model app\models\forms\PostForm */

$this->title = 'Создать пост';
$this->params['breadcrumbs'][] = ['label' => 'Моё сбалансированное питание', 'url' => [ 'diet/site/index']];
$this->params['breadcrumbs'][] = ['label' => 'Все статьи', 'url' => [ 'diet/article/index']];
$this->params['breadcrumbs'][] = 'Создать пост';
?>
<div class="post-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
