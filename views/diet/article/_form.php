<?php

use app\models\forms\blog\PostForm;
use general\ext\api\diet\DietApi;
use vova07\imperavi\Widget;
use yii\helpers\Html;
use yii\jui\DatePicker;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model PostForm */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="form">
    <?php $form = ActiveForm::begin([
	    'options' => [
		    'enctype'=>'multipart/form-data',
	    ],
    ]); ?>

	<?= $form->errorSummary($model) ?>

    <?= $form->field($model, 'nick')->textInput(['maxlength' => 100]) ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => 100]) ?>

    <?= $form->field($model, 'keywords')->textarea(['rows' => 2, 'maxlength' => 128]) ?>

    <?= $form->field($model, 'description')->textarea(['rows' => 6, 'maxlength' => 250]) ?>

	<?= $form->field($model, 'text')->widget(Widget::className(), [
		'settings' => [
			'lang' => 'ru',
			'minHeight' => 200,
			'plugins' => [
				'fullscreen',
				'video',
			],
		]
	]); ?>

	<?
	/**
	 * @param $cats
	 * @param $categories
	 * @param string $prefix
	 * @return array
	 */
	function genTree($cats, &$categories, $prefix = '') {
		$parents = [];
		foreach ($cats as $cat) {
			$parents[ $cat['name'] ] = $prefix . $cat['name'];

			if(isset($categories[ $cat['id'] ])) {
				$parents = array_merge(
					$parents,
					genTree($categories[ $cat['id'] ], $categories, $prefix . '-')
				);
			}
		}
		return $parents;
	}

	$res = DietApi::categoryOfArticleIndex();
	if($res['result'] == 'success') {
		$categories = [];
		foreach ($res['categories'] as $cat) {
			if(is_null($cat['parent_id'])) {
				$categories[0][] = $cat;
			} else {
				$categories[ $cat['parent_id'] ][] = $cat;
			}
		}

		if(isset($categories[0])) {
			$categories = genTree($categories[0], $categories);
		}
	}
	?>

	<?= $form->field($model, 'marks', [
		'labelOptions' => [
			'label' => 'Категория'
		]
	])->dropDownList($categories) ?>

    <?= $form->field($model, 'status')->dropDownList([
	    PostForm::STATUS_PUBLISHED => PostForm::$statuses[ PostForm::STATUS_PUBLISHED ],
	    PostForm::STATUS_DRAFT => PostForm::$statuses[ PostForm::STATUS_DRAFT ],
    ]) ?>

	<?= $form->field($model, 'publish_at')->widget(DatePicker::className(), [
		'model' => $model,
		'attribute' => 'publish_at',
		'language' => 'ru',
		'dateFormat' => 'dd.MM.yyyy',
		'options' => [
			'class' => 'form-control',
		],
	]) ?>

	<?= $form->field($model, 'picture')->checkbox([], false) ?>

	<? if($model->image_links) { ?>
		<?= Html::beginTag('div', ['class' => 'row']) ?>
			<?= Html::img($model->image_links['small']) ?>
		<?= Html::endTag('div') ?>
	<? } ?>

	<?= $form->field($model, 'image', [
		'template' => '{input}{error}',
		'options' => [
			'style' => 'display: none;',
		],
	])->fileInput() ?>
	<?
	$this->registerJs('(function($) {
		function picture() {
			if($("#postform-picture").prop("checked")) {
				$(".field-postform-image").show();
			} else {
				$(".field-postform-image").hide();
			}
		}
		$("#postform-picture").on("ifToggled", function() {
			picture();
		});
		picture();
	})(jQuery)');
	?>

	<?= $form->field($model, 'pic_in_post', [
		'options' => [
			'checked' => $model->isNewRecord ? 'checked' : null
		],
	])->checkbox([], false) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Изменить') ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
