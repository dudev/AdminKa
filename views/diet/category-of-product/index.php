<?php
/**
 * Project: Blog Platform - Seven Lights
 * User: Evgeny Rusanov
 * E-mail: admin@dudev.ru
 * Site: dudev.ru
 */
use app\models\search\CategoryOfArticleSearch;
use yii\grid\ActionColumn;
use yii\helpers\Html;

/* @var $this yii\web\View */
$this->title = 'Управление разделами статей';
$this->params['breadcrumbs'][] = 'Разделы статей';

/* @var yii\data\ArrayDataProvider $dataProvider */
/* @var CategoryOfArticleSearch $searchModel */
?>

<div class="menu-index">
	<p>
		<?= Html::a('Добавить раздел', ['create'], ['class' => 'btn btn-success']) ?>
	</p>

	<?= \yii\grid\GridView::widget([
		'dataProvider' => $dataProvider,
		'filterModel' => $searchModel,
		'layout' => '{items}{pager}',
		'columns' => [
			[
				'label' => $searchModel->getAttributeLabel('id'),
				'attribute' => 'id',
				'filterInputOptions' => ['style' => 'width: 50px;'],
			],
			[
				'label' => $searchModel->getAttributeLabel('name'),
				'attribute' => 'name',
			],
			[
				'label' => $searchModel->getAttributeLabel('parent_id'),
				'attribute' => 'parent_id',
			],
			[
				'label' => $searchModel->getAttributeLabel('number'),
				'attribute' => 'number',
			],
			[
				'class' => ActionColumn::className(),
				'template' => '{update} {delete}'
			],
		],
	]) ?>
</div>