<?php

use app\models\Menu;
use app\models\Service;
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\MenuSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Меню - Системный раздел';
$this->params['breadcrumbs'][] = ['label' => 'Системный раздел', 'url' => [ 'system/site/index']];
$this->params['breadcrumbs'][] = ['label' => 'Меню', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Меню';
?>
<div class="menu-index">
    <p>
        <?= Html::a('Добавить пункт', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

	<?php
	$services = [];
	$models = Service::find()->all();
	foreach ($models as $model) {
		/* @var $model Service */
		$services[ $model->id ] = $model->name;
	}

	$parents = [
		'null' => '(не задано)',
	];
	$models = Menu::find()
		->where(['parent_id' => null]);
	if($searchModel->getAttribute('service.name')) {
		$models->where(['service_id' => $searchModel->getAttribute('service.name')]);
	}
	$models = $models->all();
	foreach ($models as $model) {
		/* @var $model Service */
		$parents[ $model->id ] = $model->name;
	}
	?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
	        [
		        'attribute' => 'service.name',
		        'filter' => $services,
	        ],
	        [
		        'attribute' => 'parent.name',
		        'filter' => $parents,
	        ],
            'name',
            [
	            'attribute' => 'number',
	            'filter' => false,
            ],
            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
