<?php

use app\models\Service;
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Сервисы - Системный раздел';
$this->params['breadcrumbs'][] = ['label' => 'Системный раздел', 'url' => [ 'system/site/index']];
$this->params['breadcrumbs'][] = 'Сервисы';
?>
<div class="service-index">

    <p>
        <?= Html::a('Добавить сервис', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
	    'layout' => '{items}{pager}',
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
	        'nick',
            'name',
	        [
		        'attribute' => 'many_sites',
		        'value' => function($data) {
			        /* @var $data Service */
			        switch($data->many_sites) {
				        case (Service::MANY_SITES_ON):
					        return '<i class="fa fa-check"></i>';
				        case (Service::MANY_SITES_OFF):
					        return '<i class="fa fa-close"></i>';
			        }
		        },
		        'format' => 'raw',
	        ],
	        [
		        'attribute' => 'creator.nick',
		    ],
            [
	            'class' => 'yii\grid\ActionColumn',
	            //'template' => '{update} {delete}',
            ],
        ],
    ]); ?>

</div>
