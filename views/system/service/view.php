<?php

use yii\helpers\Html;
use yii\web\YiiAsset;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Service */

$this->title = $model->name . ' - Системный раздел';
$this->params['breadcrumbs'][] = ['label' => 'Системный раздел', 'url' => [ 'system/site/index']];
$this->params['breadcrumbs'][] = ['label' => 'Сервисы', 'url' => ['index']];
$this->params['breadcrumbs'][] = $model->name;

YiiAsset::register($this);
?>
<div class="service-view">
    <p>
        <?= Html::a('Изменить', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Вы уверены, что хотите удалить этот элемент?',
                'method' => 'post',
            ],
        ]) ?>
    </p>
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
            'nick',
	        [
		        'attribute' => 'many_sites',
		        'value' => $model->many_sites == \app\models\Service::MANY_SITES_ON ? '<i class="fa fa-check"></i>' : '<i class="fa fa-close"></i>',
		        'format' => 'raw',
	        ],
            'creator.nick',
	        'controllers',
	        [
		        'attribute' => 'created_at',
		        'format' => ['date', 'php:' . Yii::$app->params['dateFormat'] ],
	        ],
	        [
		        'attribute' => 'updated_at',
		        'format' => ['date', 'php:' . Yii::$app->params['dateFormat'] ],
	        ],
        ],
    ]) ?>
</div>
