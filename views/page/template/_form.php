<?php

use app\models\forms\page\TemplateForm;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model TemplateForm */
/* @var $form yii\widgets\ActiveForm */
/* @var $domain string */
?>

<div class="form">
    <?php $form = ActiveForm::begin([
	    'options' => [
		    'enctype'=>'multipart/form-data',
	    ],
    ]); ?>

	<?= $form->errorSummary($model) ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => 100]) ?>

    <?= $form->field($model, 'head')->textarea(['rows' => 2]) ?>

    <?= $form->field($model, 'body_start')->textarea(['rows' => 2]) ?>

    <?= $form->field($model, 'body_end')->textarea(['rows' => 2]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Изменить') ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
