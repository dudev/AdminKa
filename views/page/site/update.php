<?php
/* @var $this yii\web\View */
/* @var $model app\models\forms\page\SiteForm */

$this->title = 'Изменить сайт: ' . ' ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Страницы', 'url' => [ 'page/site/index']];
$this->params['breadcrumbs'][] = ['label' => 'Все сайты', 'url' => [ 'page/site/list']];
$this->params['breadcrumbs'][] = 'Изменить сайт';
?>
<div class="post-update">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
