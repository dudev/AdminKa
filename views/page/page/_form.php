<?php

use app\models\forms\PostForm;
use general\ext\api\page\PageApi;
use vova07\imperavi\Widget;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model PostForm */
/* @var $form yii\widgets\ActiveForm */
/* @var $domain string */
?>

<div class="form">
    <?php $form = ActiveForm::begin([
	    'options' => [
		    'enctype'=>'multipart/form-data',
	    ],
    ]); ?>

	<?= $form->errorSummary($model) ?>

    <?= $form->field($model, 'nick')->textInput(['maxlength' => 100]) ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => 100]) ?>

    <?= $form->field($model, 'keywords')->textarea(['rows' => 2, 'maxlength' => 128]) ?>

    <?= $form->field($model, 'description')->textarea(['rows' => 6, 'maxlength' => 250]) ?>

	<?= $form->field($model, 'text')->widget(Widget::className(), [
		'settings' => [
			'lang' => 'ru',
			'minHeight' => 200,
			'plugins' => [
				'fullscreen',
				'video',
			],
		]
	]); ?>

	<?
	$templates = ['Без шаблона'];
	$res = PageApi::templateList($domain);
	if($res['result'] == 'success') {
		foreach ($res['templates'] as $template) {
			$templates[ $template['id'] ] = $template['name'];
		}
	}
	?>

	<?= $form->field($model, 'template_id')->dropDownList($templates) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Изменить') ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
