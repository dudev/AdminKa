<?php
/* @var $this yii\web\View */
$this->title = 'Главная';
?>
<div id="loginForm">
    <img src="/i/logo.svg" alt="Семь огней - Seven lights" width="100">
    <div>Аутентификация прошла успешна</div>
    <p>
        <a href="<?= Yii::$app->urlManager->createUrl('site/logout') ?>">Выйти (<?= Yii::$app->user->identity->nick ?>)</a>
    </p>
</div>
