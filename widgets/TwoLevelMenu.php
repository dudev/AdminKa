<?php
/**
 * Project: Blog Platform - Seven Lights
 * User: Evgeny Rusanov
 * E-mail: admin@dudev.ru
 * Site: dudev.ru
 */

namespace app\widgets;


use yii\widgets\Menu;

class TwoLevelMenu extends Menu {
	public $items;
	public $activateParents = true;
	public function run() {
		$linksParent = [];
		foreach($this->items as $link) {
			if(is_null($link->parent_id)) {
				$linksParent[0][ $link->number ] = $link;
			} else {
				$linksParent[ $link->parent_id ][ $link->number ] = $link;
			}
		}

		//сортируем
		foreach ($linksParent as &$val) {
			ksort($val);
			$val = array_values($val);
		}

		foreach ($linksParent[0] as &$val) {
			if(isset($linksParent[ $val->id ])) {
				foreach ($linksParent[ $val->id ] as &$v) {
					$v = [
						'label' => $v->name,
						'url' => $this->generateUrl($v->route),
						'template' => '<a href="{url}">' . ($v->icon ? '<i class="fa ' . $v->icon . '"></i> ' : '') . '{label}</a>',
					];
				}
				$val = [
					'label' => $val->name,
					'url' => $this->generateUrl($val->route),
					'items' => $linksParent[ $val->id ],
					'options' => [
						'class' => 'treeview',
					],
					'template' => '<a href="{url}">' . ($val->icon ? '<i class="fa ' . $val->icon . '"></i> ' : '') . '<span>{label}</span><i class="fa fa-angle-left pull-right"></i></a>',
				];
			} else {
				$val = [
					'label' => $val->name,
					'url' => $this->generateUrl($val->route),
					'template' => '<a href="{url}">' . ($val->icon ? '<i class="fa ' . $val->icon . '"></i> ' : '') . '<span>{label}</span></a>',
				];
			}
		}

		$this->items = $linksParent[0];
		return parent::run();
	}
	private function generateUrl($json) {
		if($site_id = \Yii::$app->request->get('site')) {
			$site = ['site' => $site_id];
		} else {
			$site = [];
		}
		return array_merge(
			json_decode($json, true),
			$site
		);
	}
} 