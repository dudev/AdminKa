<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "auth_key".
 *
 * @property string $auth_key
 * @property integer $user_id
 * @property string $browser
 * @property string $ip
 * @property integer $created_at
 * @property integer $updated_at
 */
class AuthKey extends ActiveRecord
{
	public function behaviors() {
		return [
			TimestampBehavior::className(),
		];
	}
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'auth_key';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['auth_key', 'user_id', 'browser', 'ip'], 'required'],
            ['user_id', 'integer'],
            [['auth_key'], 'string', 'max' => 64],
            [['browser'], 'string', 'max' => 255],
            [['ip'], 'string', 'max' => 15]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'auth_key' => Yii::t('app', 'Auth Key'),
            'user_id' => Yii::t('app', 'User ID'),
            'browser' => Yii::t('app', 'Browser'),
            'ip' => Yii::t('app', 'IP'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }
	public function getUser() {
		return $this->hasOne(User::className(), ['id' => 'user_id']);
	}
}
