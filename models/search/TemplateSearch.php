<?php

namespace app\models\search;

use app\models\forms\page\TemplateForm;
use general\ext\api\page\PageApi;
use Yii;
use yii\data\ArrayDataProvider;

/**
 * UserSearch represents the model behind the search form about `app\models\User`.
 */
class TemplateSearch extends TemplateForm {
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [];
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ArrayDataProvider
     */
    public function search($params) {
	    if (!$this->validate()) {
		    return new ArrayDataProvider();
	    }

	    $data = PageApi::templateList($params['domain']);
	    if($data['result'] == 'success') {
		    $dataProvider = new ArrayDataProvider([
			    'models' => $data['templates'],
			    'key' => 'id',
		    ]);

		    return $dataProvider;
	    } else {
		    return new ArrayDataProvider();
	    }
    }
}
