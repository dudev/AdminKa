<?php

namespace app\models\search;

use app\models\forms\blog\PostForm;
use general\ext\api\blog\BlogApi;
use Yii;
use yii\data\ArrayDataProvider;

/**
 * UserSearch represents the model behind the search form about `app\models\User`.
 */
class PostSearch extends PostForm {
	public $id;
	public $status;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['id', 'integer'],
            ['status', 'boolean'],
        ];
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ArrayDataProvider
     */
    public function search($params) {
	    $this->setAttributes($params['PostSearch']);

	    if (!$this->validate()) {
		    return new ArrayDataProvider();
	    }

	    $data = BlogApi::postAdmin(
		    $params['domain'],
		    $params['page'],
		    $params['PostSearch'],
		    $params['sort']
	    );
	    if($data['result'] == 'success') {
		    $dataProvider = new ArrayDataProvider([
			    'models' => $data['posts'],
			    'key' => 'id',
			    'sort' => [
					'attributes' => ['id', 'title', 'status', 'publish_at'],
				],
			    'pagination' => [
				    'totalCount' => $data['totalCount'],
				    'pageSize' => 30,
			    ],
		    ]);

		    return $dataProvider;
	    } else {
		    return new ArrayDataProvider();
	    }
    }
}
