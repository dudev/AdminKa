<?php

namespace app\models\forms\page;

use Yii;
use yii\base\Model;

/**
 * This is the model class for table "post".
 *
 * @property integer $id
 * @property string $nick
 * @property string $title
 * @property string $keywords
 * @property string $description
 * @property string $text
 * @property integer $template_id
 * @property integer $site_id
 */
class PageForm extends Model {

	public $id;
	public $nick;
	public $title;
	public $keywords;
	public $description;
	public $text;
	public $template_id;
	public $site_id;

	public $isNewRecord = true;

    /**
     * @inheritdoc
     */
    public function rules() {
	    //@todo: сделать проверки на существование
        return [
	        [['nick', 'title', 'text'], 'required'],
	        ['text', 'string'],
	        ['template_id', 'integer'],
	        [['nick', 'title'], 'string', 'max' => 100],
	        [['keywords'], 'string', 'max' => 128],
	        [['description'], 'string', 'max' => 250],
	        ['template_id', 'filter', 'filter' => function($value) {
		        if(!$value) {
			        return null;
		        }
		        return $value;
	        }],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nick' => 'Мнемоника',
            'title' => 'Название',
            'keywords' => 'Ключевые слова',
            'description' => 'Описание',
            'text' => 'Текст',
            'template_id' => 'Шаблон',
        ];
    }
}
