<?php
namespace app\models\forms;

use Yii;
use yii\base\Model;

/**
 * Signup form
 */
class SignupForm extends Model {
	public $login;
	public $password;
	public $invite;

	private $_invite_models = [];

	public function rules() {
		return [
			['login', 'filter', 'filter' => 'trim'],
			['login', 'required'],
			['login', 'unique', 'targetClass' => '\app\models\User', 'message' => 'Введённый вами логин уже занят.'],
			['login', 'string', 'min' => 2, 'max' => 50],

			['password', 'required'],
			['password', 'string', 'min' => 4, 'max' => 50],

			['invite', 'required'],
			['invite', 'isSetInvite'],
		];
	}

	public function isSetInvite($attribute, $params) {
		if (!$this->hasErrors() && !$this->getInviteModel($this->invite)) {
			$this->addError($attribute,'Неверное приглашение.');
		}
	}

	/**
	 * Signs user up.
	 *
	 * @return User|null the saved model or null if saving fails
	 */
	public function signup() {

		if ($this->validate()) {
			$user = new User();
			$user->login = $this->login;
			$user->setPassword($this->password);
			$user->generateAuthKey();
			$user->save();

			$this->getInviteModel($this->invite)->delete();
			unset( $this->_invite_models[ $this->invite ] );

			return $user;
		}

		return null;
	}

	public function getInviteModel($invite) {
		if (!isset($this->_invite_models[ $invite ])) {
			$this->_invite_models[ $invite ] = Invites::findOne($this->invite);
		}

		return $this->_invite_models[ $invite ];
	}

    public function attributeLabels()
    {
        return [
            'login' => 'Логин',
            'password' => 'Пароль',
            'invite' => 'Приглашение',
        ];
    }
}
