<?php
/**
 * Project: Admin - Seven lights
 * User: Evgeny Rusanov
 * E-mail: admin@dudev.ru
 * Site: dudev.ru
 */

namespace app\models\forms;


use app\models\Service;
use general\ext\api\BaseApi;
use yii\base\Model;

class ServiceChangeForm extends Model {

	public $service;
	public $site;
	/**
	 * @inheritdoc
	 */
	public function rules() {
		return [
			['service', 'required'],
			['service', 'exist', 'targetClass' => Service::className(), 'targetAttribute' => 'nick'],
			['site', 'integer'],
			[
				'site',
				function($attribute, $params) {
					/* @var Service $model */
					if($model = Service::findOne(['nick' => $this->service])) {
						if(!$model->many_sites) {
							return;
						}
						$class = BaseApi::getInplementationName($model->nick);
						/* @var BaseApi $class */
						if ($class::siteIsSet($this->site)) {
							return;
						}
					}
					$this->addError($attribute, 'Указанный сайт не найден.');
				},
				'when' => function($model) {
					return $model->site != 0;
				},
			],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels() {
		return [
			'service' => 'Сервис',
			'site' => 'Сайт',
		];
	}
} 