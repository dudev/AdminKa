<?php
namespace app\assets;

use yii\web\AssetBundle;

class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/main.css',
	    '//cdnjs.cloudflare.com/ajax/libs/font-awesome/4.2.0/css/font-awesome.min.css',
	    '//code.ionicframework.com/ionicons/1.5.2/css/ionicons.min.css',
    ];
    public $js = [
	    'js/app.js',
    ];
    public $depends = [
        'yii\bootstrap\BootstrapPluginAsset',
	    'general\assets\JqueryUiAsset',
    ];
}
